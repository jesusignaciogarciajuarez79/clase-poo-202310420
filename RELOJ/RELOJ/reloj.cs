﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RELOJ
{
    class reloj
    {
        private int minuto = 0;
        private int segundo = 0;
        private int hora = 0;
        private int pulso = 0;
        public void setminuto(int minuto)
        {
            this.minuto = minuto;
        }
        public int getminuto()
        {
            return this.minuto;
        }
        public void setsegundo(int segundo)
        {
            this.segundo = segundo;
        }
        public int getsegundo()
        {
            return this.segundo;
        }
        public void sethora(int hora)
        {
            this.hora = hora;
        }
        public int gethora()
        {
            return this.hora;
        }
        public void setpulso(int pulso)
        {
            this.pulso = pulso;
        }
        public int getpulso()
        {
            return this.pulso;
        }
    }
}
