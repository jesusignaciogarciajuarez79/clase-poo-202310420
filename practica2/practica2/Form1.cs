﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace practica2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tv TV1 = new tv();
            TV1.settamanio(30);
            int tamanioTV1 = TV1.gettamanio();
            TV1.setvolumen(80);
            int volumenTV1 = TV1.getvolumen();
            TV1.setcontraste(12);
            int contrasteTV1 = TV1.getcontraste();
            TV1.setbrillo(20);
            int brilloTV1 = TV1.getbrillo();
            TV1.setmarca("Samsung");
            string marcaTV1 = TV1.getmarca();
            TV1.setcolor("Negra");
            string colorTV1 = TV1.getcolor();
            MessageBox.Show("El tamaño de la TV es " + tamanioTV1);
            MessageBox.Show("El Volumen de la TV es " + volumenTV1);
            MessageBox.Show("El contraste de la tv es :" + contrasteTV1);
            MessageBox.Show("El brillo de la tv es: " + brilloTV1);
            MessageBox.Show("La marca de la tv es: " + marcaTV1);
            MessageBox.Show("El color de la tv es: " + colorTV1);
        }
    }
}
